 /*******************************************************************************
  Copyright (c) 2015 Jaison Jacob, Ajmi AS ,Thomas Telkamp and Matthijs Kooijman

  Permission is hereby granted, free of charge, to anyone
  obtaining a copy of this document and accompanying files,
  to do whatever they want with them without any restriction,
  including, but not limited to, copying, modification and redistribution.
  NO WARRANTY OF ANY KIND IS PROVIDED.

  ToDo:
  - set NWKSKEY
  - set APPKSKEY
  - set DEVADDR
  - optionally comment #define DEBUG
  - optionally comment #define SLEEP
  - set TX_INTERVAL in seconds
  - change mydata to another (small) static text

*******************************************************************************/
#include <lmic.h>
#include <hal/hal.h>
#include <EnableInterrupt.h>
#include <LowPower.h>
#include <SPI.h>
#include "DHT.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

#define SLEEP
#define DEBUG

////////////////////////////////////////////////////////////////////////////////////////////////////

#define TRIGGER A0
#define DONEPIN  A1
#define SENSOR_GND 7
#define echoPin A4 // attach pin SDA Arduino to pin Echo of HC-SR04
#define trigPin A5 //attach pin SCL Arduino to pin Trig of HC-SR04
#define DHTPIN A2

////////////////////////////////////////////////////////////////////////////////////////////////////

int current_value;
int previous_value = 0;
int diff = 0;
unsigned char data[4] = {};
int distance;
long duration; // variable for the duration of sound wave travel
int f2 = 0;
float rng;
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);
bool sleep_once = false;

////////////////////////////////////////////////////////////////////////////////////////////////////

static const PROGMEM u1_t NWKSKEY[16] = { 0x10, 0xCF, 0x26, 0x3D, 0x5D, 0xAD, 0xA9, 0xE0, 0x34, 0x89, 0xA5, 0x3A, 0xAF, 0xFA, 0x30, 0x7E };
static const u1_t PROGMEM APPSKEY[16] = { 0xDE, 0xBB, 0x36, 0x17, 0x82, 0x0F, 0x46, 0x23, 0xBD, 0xA1, 0x51, 0xD0, 0x4A, 0x27, 0x63, 0xA2 };
static const u4_t DEVADDR = 0x260114b8; // <-- Change this address for every node!

void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
void(* resetFunc) (void) = 0;

struct data {
  int range;
  int voltage;
  int Humidity;
  int Temperature;
  char error;
} mydata;

static osjob_t sendjob;

#ifdef SLEEP
bool next = false;
#endif

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
//const unsigned TX_INTERVAL = 60;

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, 4},
};

void onEvent (ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch (ev) {
    case EV_SCAN_TIMEOUT:
#ifdef DEBUG
      Serial.println(F("EV_SCAN_TIMEOUT"));
#endif
    break;
    case EV_BEACON_FOUND:
#ifdef DEBUG
      Serial.println(F("EV_BEACON_FOUND"));
#endif
    break;
    case EV_BEACON_MISSED:
#ifdef DEBUG
      Serial.println(F("EV_BEACON_MISSED"));
#endif
    break;
    case EV_BEACON_TRACKED:
#ifdef DEBUG
      Serial.println(F("EV_BEACON_TRACKED"));
#endif
      break;
    case EV_JOINING:
#ifdef DEBUG
      Serial.println(F("EV_JOINING"));
#endif
    break;
    case EV_JOINED:
#ifdef DEBUG
      Serial.println(F("EV_JOINED"));
#endif
      break;
    case EV_RFU1:
#ifdef DEBUG
      Serial.println(F("EV_RFU1"));
#endif
    break;
    case EV_JOIN_FAILED:
#ifdef DEBUG
      Serial.println(F("EV_JOIN_FAILED"));
#endif
    break;
    case EV_REJOIN_FAILED:
#ifdef DEBUG
      Serial.println(F("EV_REJOIN_FAILED"));
#endif
    break;

    case EV_TXCOMPLETE:
#ifdef DEBUG
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
#endif
      if (LMIC.txrxFlags & TXRX_ACK)
#ifdef DEBUG
        Serial.println(F("Received ack"));
#endif
      if (LMIC.dataLen) {
#ifdef DEBUG
        Serial.println(F("Received "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
#endif
      }
      digitalWrite(SENSOR_GND, LOW);
      // Schedule next transmission
#ifndef SLEEP
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
#else
      next = true;
#endif
      break;
    case EV_LOST_TSYNC:
#ifdef DEBUG
      Serial.println(F("EV_LOST_TSYNC"));
#endif
      break;
    case EV_RESET:
#ifdef DEBUG
      Serial.println(F("EV_RESET"));
#endif
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
#ifdef DEBUG
      Serial.println(F("EV_RXCOMPLETE"));
#endif
      break;
    case EV_LINK_DEAD:
#ifdef DEBUG
      Serial.println(F("EV_LINK_DEAD"));
#endif
      break;
    case EV_LINK_ALIVE:
#ifdef DEBUG
      Serial.println(F("EV_LINK_ALIVE"));
#endif
      break;
    default:
#ifdef DEBUG
      Serial.println(F("Unknown event"));
#endif
      break;
  }
}

////////////////////////////////////////// do_send//////////////////////////////////////////////////

void do_send(osjob_t* j)
{
  getdist();
  delay(100);
  batt_Voltage();
  delay(100);
  readTemHum_DHT11();
  delay(100);
  
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
#ifdef DEBUG
    Serial.println(F("OP_TXRXPEND, not sending"));
#endif
  } else {
    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
#ifdef DEBUG
    Serial.println(F("Packet queued"));
#endif
  }// Next TX is scheduled after TX_COMPLETE event.
}

void setup()
{
  dht.begin();

#ifdef DEBUG
  Serial.begin(9600);
#endif
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(DONEPIN, OUTPUT);
  digitalWrite(DONEPIN, LOW);

  pinMode(TRIGGER, INPUT);
  enableInterrupt(TRIGGER, nanoTimerTrigger, FALLING);

  pinMode(SENSOR_GND, OUTPUT);

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
#ifdef PROGMEM
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
#endif
#if defined(CFG_eu868)
  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
#elif defined(CFG_us915)
  LMIC_selectSubBand(1);
#endif
  // Disable link check validation
  LMIC_setLinkCheckMode(0);
  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;
  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);

  Serial.println("******* Initialising Water Level *******");
  // Start job
  do_send(&sendjob);


}

void loop()
{
#ifndef SLEEP
  os_runloop_once();
#else

  extern volatile unsigned long timer0_overflow_count;

  if (next == false) {
    os_runloop_once();
  } else {

#ifdef DEBUG
    Serial.print(F("Enter sleeping forever "));
    Serial.flush(); // give the serial print chance to complete
#endif
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    cli();
    timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
    sei();

#ifdef DEBUG
    Serial.println(F("Sleep complete"));
#endif
  }
#endif
}

//////////////////////////////////////nano timer////////////////////////////////////////////////////

void nanoTimerTrigger() {

  next = false;
  delay(100);
  os_setCallback (&sendjob, do_send);
  digitalWrite(DONEPIN, HIGH);
  delay(1);
  digitalWrite(DONEPIN, LOW);
  delay(1);

}

////////////////////////////////////Range Measurement///////////////////////////////////////////////

void getdist()
{

  digitalWrite(SENSOR_GND, HIGH);
  delay(1000);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
 
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.0343 / 2; // Speed of sound wave divided by 2 (go and back)
  // Displays the distance on the Serial Monitor
//  return distance;
#ifdef DEBUG
  Serial.print("distance=");
  Serial.print(distance);
  Serial.println("cm");
#endif
  mydata.range = distance;
  digitalWrite(SENSOR_GND, LOW);

}

//////////////////////////////Battery///////////////////////////////////////////////////////////////

void batt_Voltage()
{
  float analogvalue = 0, battVolt = 0;
  for (byte  i = 0; i < 10; i++) {
    analogvalue += analogRead(A3);
    delay(5);
  }
  analogvalue = analogvalue / 10;
  battVolt = ((analogvalue * 3.3) / 1024) * 2; //ADC voltage*Ref. Voltage/1024
  int batt_millivolt = battVolt * 100;

#ifdef DEBUG
  Serial.print("Voltage= ");
  Serial.print(battVolt);
  Serial.println("V");
#endif

  mydata.voltage = batt_millivolt;
  batt_millivolt = 0;
  delay(1000);
}

////////////////////////////////Temperature & Humidity/////////////////////////////////////////////

void readTemHum_DHT11()
{
  delay(2000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  mydata.Humidity = h;
  mydata.Temperature = t;
  delay(100);

#ifdef DEBUG
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.println(F("°C "));
#endif

}

/////////////////////////////////////////////////////////////////////////////////////////////////
