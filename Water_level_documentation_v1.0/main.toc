\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Summary}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Problem definition and background}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Literature review}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Reference solution}{3}{section.2.2}%
\contentsline {chapter}{\numberline {3}Design}{4}{chapter.3}%
\contentsline {section}{\numberline {3.1}Architectural Overview}{4}{section.3.1}%
\contentsline {chapter}{\numberline {4}Implementation}{6}{chapter.4}%
\contentsline {section}{\numberline {4.1}Hardware}{7}{section.4.1}%
\contentsline {section}{\numberline {4.2}Electrical Connections}{10}{section.4.2}%
\contentsline {section}{\numberline {4.3}How it Works}{11}{section.4.3}%
\contentsline {section}{\numberline {4.4}Firmware}{11}{section.4.4}%
\contentsline {section}{\numberline {4.5}Testing}{11}{section.4.5}%
\contentsline {chapter}{\numberline {5}Results}{13}{chapter.5}%
\contentsline {section}{\numberline {5.1}Test 1}{13}{section.5.1}%
\contentsline {chapter}{\numberline {6}Conclusions}{15}{chapter.6}%
\contentsline {chapter}{\numberline {7}Bibliography}{16}{chapter.7}%
\contentsline {chapter}{Bibliography}{16}{chapter.7}%
