# LoRaWAN based Water Level Detector
The main scope of the proposed system should determine the level of water inside the pond.For determining the distance we use an ultrasonic sensor and transferring that data using a communication protocol to a central database for troubleshooting and analyzing.

# Getting Started
- Make sure that you have ULPLoRa board or any LoRaWAN enabled board.
- Install Arduino IDE.
- Install arduino-lmic from here . Copy the library to ~/Arduino/libraries/
- Select board : Arduino Pro Mini 3.3v 8Mhz


# Prerequisites
Arduino IDE [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

# Changelog
